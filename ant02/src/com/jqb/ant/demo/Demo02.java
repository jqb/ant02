package com.jqb.ant.demo;

public class Demo02 {
	public String hello() {
		return "hello";
	}
	
	public String world() {
		return "world";
	}
	
	public String nil() {
		return null;
	}
	
	public String notNil() {
		return "abc";
	}
	
	public String ext() {
		return null;
//		throw new NumberFormatException();
	}
}
