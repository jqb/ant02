package com.jqb.ant.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jqb.ant.demo.Demo02;

public class Test01Demo02 {
	private Demo02 demo02;

	@Before
	public void setUp() {
		demo02 = new Demo02();
	}

	@Test
	public void testHello() {
		String str = demo02.hello();
		assertEquals("hello()方法有错误", str, "hello");
	}

	@Test
	public void testWorld() {
		String str = demo02.world();
		assertEquals("world()方法有错误", str, "world");
	}

	@Test
	public void testNil() {
		assertNull("nil()方法有错误", demo02.nil());
	}

	@Test
	public void testNotNil() {
		assertNotNull("notNil()方法有错误", demo02.notNil());
	}

	@Test
	public void testExt() {
		try {
			demo02.ext();
			fail("ext()方法未抛出期待异常NumberFormatException");
		} catch (NumberFormatException e) {
		}
	}

	@After
	public void tearDown() {
		demo02 = null;
	}
}
